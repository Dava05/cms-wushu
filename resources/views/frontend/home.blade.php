<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="{{ asset('/frontend/css/style.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
</head>

<body>

    <x-navbar/>

    <header>
        <div class="ops">
            <div class="row d-flex align-items-center justify-content-end ssx">
                <div class="col-md-8 text-light text-center fw-bold mt-5">
                    <h3 class="">the 8th World Junior Wushu Championships event start</h3>
                    <h1 class="time">
                        99 : 99 : 99 : 99
                    </h1>
                    <h1 class="day">
                        Days Hours Minutes Seconds
                    </h1>
                    <h1>
                        We are the future!
                    </h1>
                    <h3>
                        See you in Indonesia
                    </h3>
                </div>
            </div>
        </div>
    </header>

    <main>
        <section id="sponsor">
            <div class="container">
                <div class="row text-center ">
                    <div class="col-md-3">
                        <h3 class="mb-3 mb-4 fw-bold text-uppercase">Global PARTNER</h3>
                        <img src="{{ asset('/frontend') }}/img/1 Global Partner1 Global Partner ºăÔ´Ïélogo 1.png"
                            class="img-fluid" alt="">
                    </div>
                    <div class="col-md-3">
                        <h3 class="mb-3 mb-4 fw-bold text-uppercase">Global SUPPORT</h3>
                        <img src="{{ asset('/frontend') }}/img/2-Global Sponsor 安踏LOGO 1.png" class="img-fluid"
                            alt="">
                    </div>
                    <div class="col-md-6 ">
                        <h3 class="mb-3 mb-4 fw-bold text-uppercase">Global SUPPLIER</h3>
                        <img src="{{ asset('/frontend') }}/img/3 1.png" class="img-fluid me-3" alt="">
                        <img src="{{ asset('/frontend') }}/img/3 2.png" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </section>
    </main>

    <x-footer/>


    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js"
        integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous">
    </script>
</body>

</html>
