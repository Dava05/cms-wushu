@php
    $routeActive = Route::currentRouteName();
@endphp

<li class="nav-item">
    <a class="nav-link {{ $routeActive == 'home' ? 'active' : '' }}" href="{{ route('home') }}">
        <i class="nav-icon fas fa-tachometer-alt text-primary"></i>
        <span class="nav-link-text">Dashboard</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link {{ $routeActive == 'users.index' ? 'active' : '' }}" href="{{ route('users.index') }}">
        <i class="nav-icon fas fa-users text-warning"></i>
        <span class="nav-link-text">Users</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link {{ $routeActive == 'news.index' ? 'active' : '' }}" href="{{ route('news.index') }}">
        <i class="nav-icon fas fa-book text-danger"></i>
        <span class="nav-link-text">News</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link {{ $routeActive == 'galeries.index' ? 'active' : '' }}" href="{{ route('galeries.index') }}">
        <i class="nav-icon fas fa-book text-primary"></i>
        <span class="nav-link-text">Galeries</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link {{ $routeActive == 'profile' ? 'active' : '' }}" href="{{ route('profile') }}">
        <i class="nav-icon fas fa-user-tie text-success"></i>
        <span class="nav-link-text">Profile</span>
    </a>
</li>