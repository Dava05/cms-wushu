@extends('layouts.app2')
@section('title', 'Tambah Data Galery')

@section('title-header', 'Tambah Data Galery')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{ route('galeries.index') }}">Data Galery</a></li>
    <li class="breadcrumb-item active">Tambah Data Galery</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card shadow">
                <div class="card-header bg-transparent border-0 text-dark">
                    <h5 class="mb-0">Formulir Tambah Data Galery</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('galeries.store') }}" method="POST" role="form" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group mb-3">
                            <label for="title">Title</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror" id="title"
                                placeholder="Title Galery" value="{{ old('title') }}" name="title">

                            @error('title')
                                <div class="d-block invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group mb-3">
                            <label for="description">Description</label>
                            <input type="text" class="form-control @error('description') is-invalid @enderror" id="description"
                                placeholder="Description Galery" value="{{ old('description') }}" name="description">

                            @error('description')
                                <div class="d-block invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group mb-3">
                            <label for="media">Image Media</label>
                            <input type="file" class="form-control dropify-media @error('media') is-invalid @enderror"
                                id="media" placeholder="Image Media Galery"
                                name="media">

                            @error('media')
                                <div class="d-block invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="form-group mb-3">
                                    <label for="parent_id">Parent Media</label>
                                    <select class="form-control @error('parent_id') is-invalid @enderror" id="parent_id" name="parent_id">
                                        <option value="" selected>---No Parent---</option>
                                        @foreach ($parents as $parent)
                                            <option value="{{ $parent->id }}" @if (old('parent_id') == $parent->id) selected @endif>
                                                {{ $parent->title ?? '-' }}</option>
                                        @endforeach
                                    </select>
        
                                    @error('parent_id')
                                        <div class="d-block invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <button type="submit" class="btn btn-sm btn-primary">Tambah</button>
                                <a href="{{route('galeries.index')}}" class="btn btn-sm btn-secondary">Kembali</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection