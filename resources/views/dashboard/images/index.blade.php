@extends('layouts.app2')
@section('title', 'Galery')

@section('title-header', 'Galery')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
    <li class="breadcrumb-item active">Galery</li>
@endsection

@section('action_btn')
    <a href="{{route('galeries.create')}}" class="btn btn-primary">Tambah Data</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card shadow">
                <div class="card-header bg-transparent border-0 text-dark">
                    <h2 class="card-title h3">Galery</h2>
                    <div class="table-responsive">
                        <table class="table table-flush table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Media</th>
                                    <th>Parent</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($images as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->title ?? '-' }}</td>
                                        <td>{{ $item->description ?? '-' }}</td>
                                        <td>
                                            <img src="{{ asset('/uploads/images/'.$item->media) }}" alt="{{ $item->title }}" width="100">
                                        </td>
                                        <td>
                                            @php
                                                $modelImage = new App\Models\Image;
                                                $parent = 'No parent';
                                                if($item->parent_id != null){
                                                    $parent = $modelImage->findOrFail($item->parent_id);
                                                    $parent = $parent->title;
                                                }
                                            @endphp
                                            {{$parent}}
                                        </td>
                                        <td class="d-flex jutify-content-center">
                                            <a href="{{route('galeries.edit', $item->id)}}" class="btn btn-sm btn-warning"><i class="fas fa-pencil-alt"></i></a>
                                            <form id="delete-form-{{ $item->id }}" action="{{ route('galeries.destroy', $item->id) }}" class="d-none" method="post">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            <button onclick="deleteForm('{{$item->id}}')" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3">Tidak ada data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="4">
                                        {{ $images->links() }}
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function deleteForm(id){
            Swal.fire({
                title: 'Hapus data',
                text: "Anda akan menghapus data!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal!'
                }).then((result) => {
                if (result.isConfirmed) {
                    $(`#delete-form-${id}`).submit()
                }
            }) 
        }
    </script>
@endsection